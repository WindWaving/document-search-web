import { Axis, Chart, Line } from 'bizcharts'
import { observer } from 'mobx-react'
import React from 'react'
import { CHARTSOURCE } from '../../constant'
import { stores } from '../../Stores'
interface IProps {
    data: any,
    axisXName:string,
    axisYName?:string,
}
@observer
export class LineChart extends React.Component<IProps>{
    // getData = () => {
    //     if (this.props.from === 'year') return stores.year.list
    //     else if (this.props.from === 'class') return stores.classify.list
    //     else if(this.props.from==='source') return stores.source.list
    // }
    // getTitle = () => {
    //     if (this.props.from === 'class') return '分类'
    //     else if (this.props.from === 'year') return '年份'
    //     else if (this.props.from === 'source') return '文章来源'
    // }
    render() {
        let { year, classify } = stores
        return (
            <div>
                <Chart
                    data={this.props.data}
                    autoFit
                    height={500}
                    // padding={[20,20]}
                    scale={{
                        key:{
                            alias:this.props.axisXName
                        },
                        value:{
                            alias:this.props.axisYName||'文章数量'
                        }
                    }}
                >
                    <Line position='key*value'></Line>
                    <Axis name='key' title></Axis>
                    <Axis name='value' title></Axis>
                </Chart>
            </div>
        )
    }
}