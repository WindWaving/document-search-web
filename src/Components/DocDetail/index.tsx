import { DownloadOutlined, PrinterOutlined, PrinterTwoTone, VerticalAlignTopOutlined } from '@ant-design/icons'
import { Anchor, BackTop, Button, Divider } from 'antd'
import { observer } from 'mobx-react'
import React from 'react'
import ReactToPrint, { useReactToPrint } from 'react-to-print'
import { htmlToPdf } from '../../Services/htmltoPdf'
import { stores } from '../../Stores'
import './style.scss'
const { Link } = Anchor

interface IState {
    cur: any,
    citeList: any,
}
@observer
export class DocDetail extends React.Component {
    state = {
        cur: { title: '', author: '', abstracts: '', source: '', content: '', reference: '' },
        citeList: []
    }
    docRef: any
    componentDidMount() {
        stores.docDetail.getCurDoc()
        this.setState({ cur: stores.docDetail.cur }, () => this.parseReference())
    }
    parseReference = () => {
        let { reference } = this.state.cur
        let pattern = /\[\d*\]/
        let res: any = reference.match(pattern)
        let labelList: any = []
        let citeList: any = []
        let last = 0
        while (res) {
            labelList.push(res[0])//[1]
            let start = +res.index + res[0].length
            // console.log('label,label结束，label开始,last开始',res[0],start,res.index,last)
            let cites = reference.substring(0, +res.index)//内容部分
            citeList.push(cites)
            last = start
            reference = reference.substr(start)//截取剩余的部分
            res = reference.match(pattern)
            // console.log('匹配',res)
        }
        citeList.push(reference)
        citeList.shift()
        let resList: any = []
        for (let i = 0; i < labelList.length; ++i) {
            resList.push({
                label: labelList[i],
                value: citeList[i]
            })
        }
        this.setState({ citeList: resList })
        // console.log('正则匹配',labelList,citeList)
    }
    downloadPdf = () => {
        let article = document.getElementById('doc-detail')
        htmlToPdf(article, this.state.cur.title)
    }
    render() {
        let { cur } = this.state
        return (

            <div className='doc-detail-wrapper'>
                <Anchor style={{marginLeft:'32px',marginTop:'100px'}}>
                    <Link href='#doc-title' title='标题' />
                    <Link href='#doc-author' title='作者' />
                    <Link href='#doc-abstract' title='摘要' />
                    <Link href='#doc-content' title='正文' />
                    <Link href='#doc-cite' title='参考文献' />
                </Anchor>
                <BackTop >
                    <div className='circle'><VerticalAlignTopOutlined /></div>
                </BackTop>
                <div className='source margin-side'>
                    {/* <span>原文出处：</sp> */}
                    <span dangerouslySetInnerHTML={{__html:cur.source}}></span>
                </div>
                <div className='btns'>
                    <ReactToPrint
                        trigger={() =>
                            <Button className='print-btn' icon={<PrinterOutlined />}>打印</Button>
                        }
                        content={() => this.docRef}
                        pageStyle='padding:16px 100px'
                    />
                    <Button className='download-btn' type='primary' onClick={this.downloadPdf} icon={<DownloadOutlined />}>
                        下载
                    </Button>
                </div>
                <div id='doc-detail' className='doc-detail-content' ref={(ref: any) => this.docRef = ref}>

                    <h1 id='doc-title' className='title' dangerouslySetInnerHTML={{__html:cur.title}}></h1>
                    <div id='doc-author' className='author'>
                        <strong>作者：</strong>
                        <span dangerouslySetInnerHTML={{__html:cur.author}}></span>
                    </div>

                    <div id='doc-abstract' className='g-indent-para'>
                        <strong>摘 <span>&nbsp;&nbsp;&nbsp;&nbsp;</span> 要：</strong>
                        <span dangerouslySetInnerHTML={{__html:cur.abstracts}}></span>
                    </div>
                    <Divider />
                    <article id='doc-content' dangerouslySetInnerHTML={{ __html: cur.content }} className='g-indent-para'>
                    </article>
                    <div id='doc-cite' className='cite-wrapper'>
                        <h4 className='cite-header'>参考文献</h4>
                    </div>
                    {
                        this.state.citeList.map((item: any) => (
                            <p>{item.label}{item.value}</p>
                        ))
                    }
                </div>
            </div>
        )
    }
}