import { Axis, Chart, Interval, Line } from 'bizcharts'
import { observer } from 'mobx-react'
import React from 'react'
import { CHARTSOURCE } from '../../constant'
import { stores } from '../../Stores'
interface IProps {
    data: any,
    axisXName: string,
    axisYName?: string,
}
@observer
export class TreeChart extends React.Component<IProps>{
    render() {
        return (
            <div>
                <Chart
                    data={this.props.data}
                    autoFit
                    height={500}
                    scale={{
                        key: {
                            alias: this.props.axisXName
                        },
                        value: {
                            alias: this.props.axisYName || '文章数量'
                        }
                    }}
                >
                    <Interval
                        position="key*value"
                    />
                    <Axis name='key' title></Axis>
                    <Axis name='value' title></Axis>
                </Chart>
            </div>
        )
    }
}