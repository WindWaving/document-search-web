import { Divider } from 'antd'
import React from 'react'
import './style.scss'

export class Footer extends React.Component {
    render() {
        return (
            <div className='footer'>
                <div className='links-area'>
                    <ul className='links-ul'>
                        <li className='footer-link'>友情链接</li>
                        <li className="footer-link"><a target='_blank' href="https://webvpn.bit.edu.cn/https/77726476706e69737468656265737421e7e056d235346e497c03c7af9758/home/allsearch">人大复印报刊资料</a></li>
                        <li className="footer-link"><a target='_blank' href="https://webvpn.bit.edu.cn/https/77726476706e69737468656265737421e7e056d2243e635930068cb8/">中国知网</a></li>
                        <li className="footer-link"><a target='_blank' href="https://webvpn.bit.edu.cn/https/77726476706e69737468656265737421f7b9569d2936695e790c88b8991b203a18454272/index.html">万方数据</a></li>
                        <li className="footer-link"><a target='_blank' href="https://webvpn.bit.edu.cn/http/77726476706e69737468656265737421e7e056d2223e6f59700d8cbe915b24239d8e8349f501643293dc/?wrdrecordvisit=1635390338000">EI</a></li>
                        <li className="footer-link"><a target='_blank' href="https://webvpn.bit.edu.cn/https/77726476706e69737468656265737421e7e056d230356a5f781b8aa59d5b20301c1db852/wos/woscc/search-with-editions?editions=WOS.SCI">SCI</a></li>
                    </ul>
                </div>
                <div className='end-footer'>
                    北京理工大学计算机学院软件工程
                </div>
            </div>
        )
    }
}