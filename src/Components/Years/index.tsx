import { LineChartOutlined } from '@ant-design/icons'
import { Button, Card, Checkbox, Collapse, Modal, Pagination, Spin } from 'antd'
import { observer } from 'mobx-react'
import React from 'react'
import { stores } from '../../Stores'
import { LineChart } from '../LineChart'
import './style.scss'

interface IState {
    checked: any
    isShowChart: boolean,
}
@observer
export class Years extends React.Component<{}, IState> {
    state = {
        checked: [],
        isShowChart: false,
    }
    componentDidMount() {
        stores.year.generateYears()
    }
    toShowChart = () => { 
        this.setState({isShowChart:true})
    }
    render() {
        let { year } = stores
        return (
            <Card title={<span className='g-card-title'>年份</span>}
                className='g-card-wrapper'
                bordered={false}
                extra={<LineChartOutlined className='g-span-link' onClick={this.toShowChart} />}
            >
                <div className='years-wrapper'>
                    <Spin spinning={year.isLoading}>
                        <Collapse bordered={false} ghost>
                            {
                                Object.keys(year.list).map((label: string) => (
                                    <Collapse.Panel key={label} header={<span>{label}</span>}>
                                        <Checkbox.Group value={year.checkedYears} onChange={(checked: any) => year.setCheckedYears(checked)}>
                                            <div className='years-content'>
                                                {
                                                    year.list[label].map((item: any) => (
                                                        <Checkbox value={item.key} key={item.key}>
                                                            <span className='a-year'>{item.key}({item.value})</span>
                                                        </Checkbox>
                                                    ))
                                                }
                                            </div>
                                        </Checkbox.Group>
                                    </Collapse.Panel>
                                ))
                            }
                        </Collapse>
                    </Spin>

                </div>
            </Card>
        )
    }
}