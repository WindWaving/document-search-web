import { MinusCircleFilled, RightOutlined } from '@ant-design/icons'
import { observer } from 'mobx-react'
import React from 'react'
import { stores } from '../../Stores'
import { SEARCHTYPE } from '../../Stores/search'
import './style.scss'

@observer
export class SearchMenu extends React.Component {
    getType = (type: string) => {
        switch (type) {
            case SEARCHTYPE.keywords: return '主题词'
            case SEARCHTYPE.abstracts: return '摘要'
            case SEARCHTYPE.author: return '作者'
            case SEARCHTYPE.content: return '全文'
            case SEARCHTYPE.source: return '原文出处'
            case SEARCHTYPE.title: return '标题'
        }
    }
    deleteLastSearch = () => {
        let {search}=stores
        search.deleteResultSearchTab()
        let len=search.resultSearchTabs.length
        search.setSearchType(search.resultSearchTabs[len-1].type)
        search.setSearchValue(search.resultSearchTabs[len-1].value)
        stores.classify.setCheckedClass([])
        stores.year.setCheckedYears([])
        stores.source.setChecked([])
        stores.classify.setInit(true)
        search.resSearchForList()
    }
    render() {
        let { search } = stores
        let len = search.resultSearchTabs.length
        return (
            <div className='search-menu-bar'>
                <span className='g-small-font'>检索过程：</span>
                {
                    search.resultSearchTabs.map((item: any, i: number) => (
                        <span key={i}>
                            <span className='g-small-font'>{this.getType(item.type)}</span>
                            <span className='g-small-font'>：</span>
                            <span className='g-small-font'>{item.value}</span>
                            {i == len - 1 && i !== 0 && <MinusCircleFilled className='g-smaller-font small-sept' onClick={this.deleteLastSearch} />}
                            {i < len - 1 && <span className='g-small-font sept'>AND</span>}
                        </span>
                    ))
                }
            </div>
        )
    }
}