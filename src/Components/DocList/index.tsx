import { Divider, Empty, Space, Spin, Table } from "antd";
import { observer } from "mobx-react";
import React from "react";
import { Link, withRouter } from "react-router-dom";
import { Urls } from "../../constant";
import { stores } from "../../Stores";
import { SEARCHTYPE } from "../../Stores/search";
import './style.scss'

@observer
class DocListC extends React.Component<any> {
    onClickAuthor = (author: any) => {
        let { pathname } = window.location
        if (pathname != Urls.homePage) {
            this.props.history.push(Urls.homePage)
        }
        stores.search.setSearchType(SEARCHTYPE.author)
        stores.search.setSearchValue(author)
    }
    onClickSource = (source: any) => {
        let { pathname } = window.location
        if (pathname != Urls.homePage) {
            this.props.history.push(Urls.homePage)
        }
        stores.search.setSearchType(SEARCHTYPE.source)
        stores.search.setSearchValue(source)
    }
    onChangePage = (page: number) => {
        let { listFromType } = stores.docList
        stores.docList.setPage(page)
        if (listFromType === 'single') {
            stores.search.directSearchForList()
            console.log('简单搜索', page)
        }
        else if (listFromType === 'process') {
            stores.search.resSearchForList()
            console.log('过程中检索', page)
        } else if (listFromType === 'senior') {
            stores.seniorSearch.searchForList()
            console.log('高级检索', page)
        }
    }
    render() {
        let { docList, docDetail } = stores
        return (
            <Spin spinning={docList.isLoading}>
                <Table
                    dataSource={docList.list}
                    columns={[
                        {
                            title: '题名',
                            dataIndex: 'title',
                            width:340,
                            render: (text: string, record: any) =>
                                <Link to={Urls.detail}
                                    // style={{ color: 'black' }}
                                    onClick={() => docDetail.setCurDoc(record)}
                                    target='_blank'
                                    className='g-table-link table-title'
                                    dangerouslySetInnerHTML={{ __html: record.title }}
                                >
                                    {/* <span dangerouslySetInnerHTML={{__html:record.title}}></span> */}
                                </Link>
                        },
                        {
                            title: '作者',
                            dataIndex: 'author',
                            render: (text: string, record: any) => (
                                <Space split={<Divider type='vertical' />}>
                                    <span dangerouslySetInnerHTML={{__html:record.author}}></span>
                                    {/* {
                                        record.author.split('/').map((item: any) => (
                                            <div key={item}
                                                className='g-table-link g-smaller-font'
                                                onClick={() => this.onClickAuthor(item)}
                                                dangerouslySetInnerHTML={{ __html: item }}></div>
                                        ))
                                    } */}
                                </Space>
                            )
                        },
                        {
                            title: '来源',
                            dataIndex: 'source',
                            render: (text: string, record: any) => <span className='g-table-link g-smaller-font' onClick={() => this.onClickSource(record.source)}>{record.source}</span>
                        },
                        { title: '年份', dataIndex: 'year', render: (text: string) => <span className='g-smaller-font'>{text}</span> },
                        { title: '分类', dataIndex: 'classname',width:80, render: (text: string) => <span className='g-smaller-font' dangerouslySetInnerHTML={{__html:text}}></span> }
                    ]}
                    style={{ marginTop: '16px' }}
                    pagination={
                        {
                            current: docList.page,
                            total: docList.total,
                            hideOnSinglePage: true,
                            onChange: (page, size) => this.onChangePage(page),
                        }
                    }
                ></Table>
            </Spin>
        )
    }
}

export const DocList = withRouter(DocListC)