import { DownOutlined, LineChartOutlined, UpOutlined } from '@ant-design/icons'
import { Button, Card, Checkbox, Col, Collapse, Modal, Pagination, Row, Spin } from 'antd'
import { observer } from 'mobx-react'
import React from 'react'
import { CHARTSOURCE } from '../../constant'
import { stores } from '../../Stores'
import { LineChart } from '../LineChart'
import { PieChart } from '../PieChart'
import { TreeChart } from '../TreeChart'
import './style.scss'


interface IProps {
    from: CHARTSOURCE
}
interface IState {
    checked: any,
    isShowChart: boolean,
}
@observer
export class Classifications extends React.Component<IProps, IState> {
    state = {
        checked: [],
        isShowChart: false,
    }
    async componentDidMount() {
        // if (this.props.from === 'class') await stores.classify.getClassNames()
        // else if (this.props.from === 'year') stores.year.generateYears()
        // else if (this.props.from === 'source') stores.source.getSourceList()
    }
    toShowChart = (e: any) => {
        this.setState({ isShowChart: true })
        e.stopPropagation()
    }
    getData = () => {
        if (this.props.from === 'class') return stores.classify.list
        else if (this.props.from === 'year') return stores.year.list
        else if (this.props.from === 'source') return stores.source.list
    }
    getTitle = () => {
        if (this.props.from === 'class') return '分类'
        else if (this.props.from === 'year') return '年份'
        else if (this.props.from === 'source') return '文章来源'
        return ''
    }
    getChecked = () => {
        if (this.props.from === 'class') return stores.classify.checkedClasses
        else if (this.props.from === 'year') return stores.year.checkedYears
        else if (this.props.from === 'source') return stores.source.checked
    }
    getIsWhole = () => {
        if (this.props.from === 'class') return stores.classify.isWhole
        else if (this.props.from === 'year') return stores.year.isWhole
        else if (this.props.from === 'source') return stores.source.isWhole
    }
    setWhole=(whole:boolean)=>{
        if (this.props.from === 'class') return stores.classify.setWhole(whole)
        else if (this.props.from === 'year') return stores.year.setWhole(whole)
        else if (this.props.from === 'source') return stores.source.setWhole(whole)
    }
    onChange = (checked: any) => {
        if (this.props.from === 'class') stores.classify.setCheckedClass(checked)
        else if (this.props.from === 'year') stores.year.setCheckedYears(checked)
        else if (this.props.from === 'source') stores.source.setChecked(checked)
    }
    render() {
        let { classify, year, source } = stores
        let { from } = this.props
        return (
            // <Collapse ghost bordered>
            // {/* <Collapse.Panel
            //         header={<>
            //             <span className='g-card-title'>{this.getTitle()}
            //             <LineChartOutlined className='g-span-link' onClick={this.toShowChart} /></span>

            //         </>}
            //         key={from}
            //     > */}
            < Card title={<span className='g-card-title' > {this.getTitle()}</span >}
                className='g-card-wrapper sider-bar-card'
                style={{ marginBottom: '24px' }}
                bordered={false}
                extra={
                    <span>
                        < LineChartOutlined style={{ color: '#7089b7' }} className='g-span-link' onClick={this.toShowChart} />
                        {!this.getIsWhole()&& <DownOutlined style={{ color: '#7089b7' }} className='g-span-link' onClick={() => this.setWhole(true)} />}
                        {this.getIsWhole() && <UpOutlined style={{ color: '#7089b7' }} className='g-span-link' onClick={() => this.setWhole(false)} />}
                    </span>}
            >
                <Spin spinning={classify.loading}>
                    <div className='classes-wrapper'>
                        <Checkbox.Group value={this.getChecked()}
                            onChange={this.onChange}
                        >
                            {
                                from === 'class' &&
                                <div className='average'>{
                                    classify.viewList.map((item: any) => (
                                        <Checkbox key={item.key} value={item.key}>
                                            <span className='a-class'>{item.key}({item.value})</span>
                                        </Checkbox>
                                    ))}</div>
                            }
                            {
                                from === 'year' &&
                                <div className='average'>{
                                    year.viewList.map((item: any) => (
                                        <Checkbox key={item.key} value={item.key}>
                                            <span className='a-class'>{item.key}({item.value})</span>
                                        </Checkbox>
                                    ))
                                }</div>
                            }
                            {
                                from === 'source' &&
                                <div className='whole-line'>{
                                    source.viewList.map((item: any) => (
                                        <Checkbox key={item.key} value={item.key}>
                                            <span className='a-class'>{item.key}({item.value})</span>
                                        </Checkbox>
                                    ))
                                }</div>
                            }
                        </Checkbox.Group>
                    </div>
                </Spin>
                <Modal
                    visible={this.state.isShowChart}
                    footer={null}
                    onCancel={() => this.setState({ isShowChart: false })}
                    width={900}
                >
                    {
                        from === 'class' && <TreeChart
                            data={classify.list}
                            axisXName={this.getTitle()}
                        />
                    }
                    {from === 'year' && <LineChart
                        data={year.list}
                        axisXName={this.getTitle()}
                    />}
                    {
                        from === 'source' && <PieChart
                            data={source.list}
                            axisXName={this.getTitle()}
                        />
                    }
                </Modal>
            </ Card >
            //     </Collapse.Panel>
            // </Collapse>
        )
    }
}