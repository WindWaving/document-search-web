import { observer } from 'mobx-react'
import React from 'react'
import { stores } from '../../Stores'
import { SORT } from '../../Stores/search'
import './style.scss'

@observer
export class SortMenu extends React.Component{
    onSort=(sort:any)=>{
        stores.docList.setSort(sort)
        stores.docList.setPage(1)
        let {listFromType}=stores.docList
        if(listFromType==='single'){
            stores.search.directSearchForList()
        }else if(listFromType==='process'){
            stores.search.resSearchForList()
        }else if(listFromType==='senior'){
            stores.seniorSearch.searchForList()
        }
    }
    render(){
        let {search}=stores
        return(
            <div className='sort-menu-bar'>
                <span>排序：</span>
                <span className='g-span-link' onClick={()=>this.onSort(SORT.relative)}>相关度</span>
                <span className='g-span-link' onClick={()=>this.onSort(SORT.year)}>年份</span>
            </div>
        )
    }
}