import { Axis, Chart, Coordinate, Interval, Tooltip } from 'bizcharts'
import { observer } from 'mobx-react'
import React from 'react'

interface IProps {
    data: any,
    axisXName: string,
    axisYName?: string,
}
@observer
export class PieChart extends React.Component<IProps>{
    render() {
        return (
            <Chart
                data={this.props.data}
                height={500}
                autoFit
                scale={{
                    key: {
                        alias: this.props.axisXName
                    },
                    value: {
                        alias: this.props.axisYName || '文章数量'
                    }
                }}
            >
                <Tooltip showTitle={false} />
                <Coordinate type="polar" />
                <Interval
                    position='key*value'
                    color='key'
                    adjust="stack"
                    element-highlight
                    // label={['key', {
                    //     offset: -15,
                    // }]}
                />
                {/* <Axis name='key' title></Axis> */}
                <Axis name='value' title></Axis>
            </Chart>
        )
    }
}