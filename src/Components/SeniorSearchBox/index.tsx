import { Input, Row, Select, Form, DatePicker, Col, Button, Card } from 'antd'
import { observer } from 'mobx-react'
import moment from 'moment'
import React from 'react'
import { stores } from '../../Stores'
import { SEARCHTYPE, SORT } from '../../Stores/search'
import { MinusOutlined, PlusOutlined } from '@ant-design/icons'
import 'moment/locale/zh-cn';
import locale from 'antd/es/date-picker/locale/zh_CN';
import { FUZZY, LOGICOPT } from '../../Stores/seniorSearch'
import './style.scss'
const { Option } = Select

@observer
export class SeniorSearchBox extends React.Component {
    onSeniorSearch=()=>{
        stores.docList.setPage(1)
        stores.docList.setSort(SORT.none)
        stores.docList.setListFromType('senior')
        stores.seniorSearch.searchForList()
    }
    render() {
        let { seniorSearch } = stores
        return (
            <Card title='高级检索' bordered className='card-wrapper'>
                {
                    seniorSearch.searchList.map((item: any, index: number) => (
                        index > 0 && <Row gutter={24} justify='space-between' style={{ display: 'flex', alignItems: 'center', marginTop: '16px' }}>
                            <Col span={4}>
                            </Col>
                            <Col span={2}>
                                {index > 1 && <Select
                                    value={item.logic}
                                    style={{width:'100%'}}
                                    onChange={(value: any) => seniorSearch.setSearchList(index, item.type, item.keywords, item.isFuzzy, value)}
                                >
                                    <Option value={LOGICOPT.and}>AND</Option>
                                    <Option value={LOGICOPT.or}>OR</Option>
                                    <Option value={LOGICOPT.not}>NOT</Option>
                                </Select>}
                            </Col>
                            <Col span={10}>
                                <Input.Group compact>
                                    <Select
                                        style={{ width: '20%', textAlign: 'center' }}
                                        value={item.type}
                                        onChange={(value: string) => seniorSearch.setSearchList(index, value, item.keywords, item.isFuzzy, item.logic)}>
                                        {/* <Option value={SEARCHTYPE.keywords}>主题词</Option> */}
                                        <Option value={SEARCHTYPE.title}>标题</Option>
                                        <Option value={SEARCHTYPE.author}>作者</Option>
                                        <Option value={SEARCHTYPE.abstracts}>摘要</Option>
                                        <Option value={SEARCHTYPE.source}>原文出处</Option>
                                        <Option value={SEARCHTYPE.content}>全文</Option>
                                        <Option value={SEARCHTYPE.classname}>分类</Option>
                                    </Select>
                                    <Input
                                        style={{ width: '60%' }}
                                        value={item.keywords}
                                        onChange={(e: any) => seniorSearch.setSearchList(index, item.type, e.target.value, item.isFuzzy, item.logic)}
                                    ></Input>
                                    <Select
                                        style={{ width: '20%', textAlign: 'center' }}
                                        value={item.isFuzzy}
                                        onChange={(value: any) => seniorSearch.setSearchList(index, item.type, item.keywords, value, item.logic)}
                                    >
                                        <Option value={FUZZY.accurate}>精确</Option>
                                        <Option value={FUZZY.similar}>模糊</Option>
                                    </Select>
                                </Input.Group>
                            </Col>
                            {
                                <Col span={2}>
                                    {index < 3 && <PlusOutlined className='g-clickable-icon'
                                        style={{ marginRight: '10px' }}
                                        onClick={() => seniorSearch.addSearchItem()}
                                    />}
                                    {
                                        index > 1 && <MinusOutlined className='g-clickable-icon'
                                            onClick={() => seniorSearch.deleteSearchItem(index)} />
                                    }
                                </Col>
                            }
                            <Col span={4}></Col>
                        </Row>
                    ))
                }
                <Row gutter={24} style={{ marginTop: '24px' }} justify='space-between'>
                    <Col span={4}></Col>
                    <Col span={3}><span style={{ fontWeight: 'bold' }}>时间范围：</span></Col>
                    <Col span={11}>
                        <DatePicker.RangePicker
                            locale={locale}
                            picker='year'
                            value={[moment(new Date(stores.seniorSearch.searchList[0].keywords.split(';')[0])), moment(new Date(stores.seniorSearch.searchList[0].keywords.split(';')[1]))]}
                            onChange={(dates: any) => seniorSearch.setSearchList(0, 'year', `${new Date(dates[0]).getFullYear()};${new Date(dates[1]).getFullYear()}`, FUZZY.accurate, LOGICOPT.and)}
                        ></DatePicker.RangePicker>
                    </Col>
                    <Col span={4}></Col>
                </Row>
                <div style={{ width: '100%',textAlign:'center',marginTop:'32px'}}>
                    <Button type='primary' onClick={this.onSeniorSearch}>检索</Button>
                </div>
            </Card>
        )
    }
}