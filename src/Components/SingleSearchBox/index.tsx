import { SearchOutlined } from '@ant-design/icons'
import { Button, Input, message, Modal, Select } from 'antd'
import { observer } from 'mobx-react'
import React from 'react'
import { Link } from 'react-router-dom'
import { Urls } from '../../constant'
import { stores } from '../../Stores'
import { SEARCHTYPE, SORT } from '../../Stores/search'
import './style.scss'
const { Option } = Select

interface IState {
    searchType: string,
    searchValue: string,
}
@observer
export class SingleSearchBox extends React.Component<{}, IState> {
    state = {
        searchType: SEARCHTYPE.keywords,
        searchValue: '',
    }
    initSearch = () => {
        stores.docList.setPage(1)
        stores.docList.setSort(SORT.none)
    }
    resultSearch = () => {
        this.initSearch()
        let { search: { searchValue, searchType } } = stores
        stores.search.addResultSearchTab(searchType, searchValue)
        stores.docList.setListFromType('process')
        stores.classify.setCheckedClass([])
        stores.year.setCheckedYears([])
        stores.source.setChecked([])
        stores.search.resSearchForList()
        stores.classify.setInit(true)
    }
    directSearch = () => {
        let { search: { searchValue, searchType } } = stores
        if (!searchValue) {
            message.error({
                content: '请输入搜索词',
                duration: 1
            })
            return;
        }
        this.initSearch()
        stores.docList.setListFromType('single')
        stores.search.setResultSearchTab([{ type: searchType, value: searchValue }])
        stores.classify.setCheckedClass([])
        stores.year.setCheckedYears([])
        stores.source.setChecked([])
        stores.search.directSearchForList()
        stores.classify.setInit(true)
    }

    render() {
        let { search } = stores
        let len = search.resultSearchTabs.length
        return <div className='single-search-box-wrapper'>
            <Input.Group compact className='search-box'>
                <Select size='large'
                    style={{ width: '15%', textAlign: 'center' }}
                    value={search.searchType}
                    onChange={(value: string) => search.setSearchType(value)}>
                    <Option value={SEARCHTYPE.keywords}>主题词</Option>
                    <Option value={SEARCHTYPE.title}>标题</Option>
                    <Option value={SEARCHTYPE.author}>作者</Option>
                    <Option value={SEARCHTYPE.abstracts}>摘要</Option>
                    <Option value={SEARCHTYPE.source}>原文出处</Option>
                    <Option value={SEARCHTYPE.content}>全文</Option>
                </Select>
                <Input.Search
                    size='large'
                    style={{ width: '75%' }}
                    value={search.searchValue}
                    onChange={(e: any) => search.setSearchValue(e.target.value)}
                    enterButton={<SearchOutlined />}
                    // allowClear
                    onSearch={this.directSearch}
                    placeholder="乡村、人工智能、图书馆、建筑、新时代、马克思、法律、体育、汽车"
                />
            </Input.Group>
            <Button type='primary' size='large' style={{ marginLeft: '16px' }} onClick={() => this.resultSearch()}>结果中检索</Button>
            <Link to={Urls.seniorSearch} className='to-senior-link'>
                高级搜索
            </Link>
        </div>
    }
}