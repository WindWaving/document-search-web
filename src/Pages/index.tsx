import { Route, Switch } from "react-router-dom"
import React from "react";
import { HomePage } from "./HomePage";
import { Detail } from "./Detail";
import { Urls } from "../constant";
import { SeniorSearch } from "./SeniorSearch";
export class Page extends React.Component{
    render(){
        return <Switch>
            <Route path={Urls.homePage} component={HomePage}></Route>
            <Route path={Urls.detail} component={Detail}></Route>
            <Route path={Urls.seniorSearch} component={SeniorSearch}></Route>
        </Switch>
    }
}