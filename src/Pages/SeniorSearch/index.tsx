import { Empty } from 'antd'
import { observer } from 'mobx-react'
import React from 'react'
import { DocList } from '../../Components/DocList'
import { Header } from '../../Components/Header'
import { SeniorSearchBox } from '../../Components/SeniorSearchBox'
import { SortMenu } from '../../Components/SortMenu'
import { stores } from '../../Stores'
import './style.scss'

@observer
export class SeniorSearch extends React.Component {
    componentDidMount(){
        stores.docList.setDocList([])
    }
    render() {
        let { docList } = stores
        return (
            <div>
                <Header />
                <div className='senior-search-wrapper'>
                    <SeniorSearchBox />
                </div>
                {!docList.list.length && <Empty description=''/>}
                {!!docList.list.length && <div className='g-table-area' style={{margin:'0 100px'}}>
                    <SortMenu />
                    <DocList />
                </div>}
            </div>
        )
    }
}