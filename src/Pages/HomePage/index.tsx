import { Header } from '../../Components/Header'
import React from 'react'
import { SingleSearchBox } from '../../Components/SingleSearchBox'
import { DocList } from '../../Components/DocList'
import { Button, Col, Empty, message, Row } from 'antd'
import { Classifications } from '../../Components/Classifications'
import { Years } from '../../Components/Years'
import { SearchMenu } from '../../Components/SearchMenu'
import { SortMenu } from '../../Components/SortMenu'
import { stores } from '../../Stores'
import { observer } from 'mobx-react'
import './style.scss'
import { Footer } from '../../Components/Footer'

@observer
export class HomePage extends React.Component {
    onSiderSearch=()=>{
        if(!stores.search.searchValue){
            message.error({
                content:'未输入搜索词',
                duration:1
            })
            return;
        }
        stores.classify.setInit(true)
        stores.search.resSearchForList()
    }
    onReset=()=>{
        stores.classify.setCheckedClass([])
        stores.year.setList([])
        stores.source.setList([])
    }
    render() {
        let { docList } = stores
        return (
            <div>
                <Header />
                <SingleSearchBox />
                <Row gutter={24} justify='space-around' style={{ padding: '0 160px',position:'relative' }}>
                    <div className='fixed-sider-btn sure' onClick={this.onSiderSearch}>确定</div>
                    <div className='fixed-sider-btn reset' onClick={this.onReset}>重置</div>
                    <Col span={6}>
                        <div className='sider-bar'>
                        {/* <div style={{ textAlign: 'center', marginTop: '16px' }}>
                                <Button type='primary' onClick={this.onSiderSearch}>提交</Button>
                            </div> */}
                            <Classifications from='class' />
                            <Classifications from='year' />
                            <Classifications from='source' />
                            {/* <Years /> */}
                            
                        </div>
                    </Col>
                    <Col span={18}>
                        <div className='g-table-area'>
                            <SearchMenu />
                        </div>
                        {!docList.list?.length && <Empty description='暂无数据' 
                        className='empty-area'
                        image='https://webvpn.bit.edu.cn/https/77726476706e69737468656265737421e7e056d235346e497c03c7af9758/img/nojg.png' />}
                        {!!docList.list?.length && <div className='g-table-area'>
                            <SortMenu />
                            <DocList />
                        </div>}

                    </Col>
                </Row>
                <Footer />
            </div>
        )
    }
}