import React from 'react'
import { DocDetail } from '../../Components/DocDetail'
import { Header } from '../../Components/Header'

export class Detail extends React.Component{
    render(){
        return(
            <div>
                <Header />
                <DocDetail />
            </div>
        )
    }
}