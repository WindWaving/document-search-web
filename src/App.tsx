import React from 'react';
import logo from './logo.svg';
import './App.css';
import { Page } from './Pages';
import { BrowserRouter } from 'react-router-dom';

function App() {
  return (
    <BrowserRouter>
      <Page />
    </BrowserRouter>
  );
}

export default App;
