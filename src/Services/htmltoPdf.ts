import html2canvas from "html2canvas";
import jsPDF from "jspdf";

export async function htmlToPdf(html: any,fileName:string) {
    let res = await html2canvas(html, {
        useCORS: true,
        allowTaint: true
    })
    const width = res.width
    let height = res.height
    // 一页pdf需要canvas的高度
    const pageHgt = (res.width / 592.28) * 841.89
    // 页面偏移
    let pos = 0
    const imgWidth = 595.28
    const imgHgt = (imgWidth / width) * height
    const pageData = res.toDataURL('image/jpeg', 1.0)
    const pdf = new jsPDF('portrait', 'pt', 'a4')
    if (height < pageHgt) {
        pdf.addImage(pageData, 'JPEG', 0, 0, imgWidth, imgHgt)
    } else {
        while (height > 0) {
            pdf.addImage(pageData, 'JPEG', 0, pos, imgWidth, imgHgt);
            height -= pageHgt;
            pos -= 841.89;
            // 避免添加空白页
            if (height > 0) {
                pdf.addPage();
            }
        }
    }
    pdf.save(`${fileName}.pdf`)
}