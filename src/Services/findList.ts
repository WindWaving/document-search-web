import { request } from "../request";

interface ProcessQueryEntity{
    type:string,
    value:string,
}
interface ProcessQueryParam{
    page:number,
    size:number,
    sort:number,
    query:ProcessQueryEntity[],
}
interface SingleSearchEntity{
    value:string,
    page:number,
    size:number,
    sort:number
}
export function getListByProcess(param:any){
    return request('/processFind',{
        method:'POST',
        data:param
    })
}
export function getListBySeniorSearch(param:any){
    return request('/senFind',{
        method:'POST',
        data:param
    })
}
export function getListByTitle(params:SingleSearchEntity){
    return request('/getByTitle',{
        params
    })
}
export function getListByAuthor(params:SingleSearchEntity){
    return request('/getByAuthor',{
        params
    })
}

export function getListBySource(params:SingleSearchEntity){
    return request('/getBySource',{
        params
    })
}
export function getListByAbstract(params:SingleSearchEntity){
    return request('/getByAbstracts',{
        params
    })
}
export function getListByContent(params:SingleSearchEntity){
    return request('/getByContent',{
        params
    })
}
export function getListByKey(params:SingleSearchEntity){
    return request('/keywordsFind',{
        params
    })
}