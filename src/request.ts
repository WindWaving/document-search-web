import axios, { AxiosRequestConfig } from 'axios';
import { stores } from './Stores';
const instance = axios.create({
    baseURL: 'http://localhost:8080/api/document',
    // withCredentials: true
});
instance.interceptors.response.use((response:any)=>{
    return response;
})
export const request = instance