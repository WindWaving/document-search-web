import { makeAutoObservable, runInAction } from "mobx";
import { LISTFROMTYPE } from "../constant";
import { SORT } from "./search";

export class DocList{
    constructor(){
        makeAutoObservable(this)
    }
    list:any=[]
    setDocList(list:any){
        // console.log('list',list)
        runInAction(()=>{
            this.list=list
        })
    }
    total:number=0
    setTotal(total:number){
        runInAction(()=>this.total=total)
    }
    page:number=1
    /**
     * 
     * @param page page从1开始
     */
    setPage(page:number){
        runInAction(()=>this.page=page)
    }
    size:number=10
    sort:number=SORT.none
    setSort(sort:any){
        runInAction(()=>this.sort=sort)
    }
    listFromType:LISTFROMTYPE='single'
    setListFromType(type:LISTFROMTYPE){
        runInAction(()=>this.listFromType=type)
    }
    isLoading:boolean=false
    setLoading(load:boolean){
        runInAction(()=>this.isLoading=load)
    }
}