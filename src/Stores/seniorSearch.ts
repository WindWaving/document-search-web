import { makeAutoObservable, runInAction } from "mobx";
import { stores } from ".";
import { TESTLIST } from "../constant";
import { getListBySeniorSearch } from "../Services/findList";
import { SEARCHTYPE } from "./search";

export const LOGICOPT = {
    and: 0,
    or: 1,
    not: 2
}
export const FUZZY = {
    accurate: 1,
    similar: 0
}
export class SeniorSearch {
    constructor() {
        makeAutoObservable(this)
    }
    //与或非：012，精确:1,模糊：0
    searchList: any = [
        {
            type: 'year', keywords: '1995;2021', isFuzzy: FUZZY.accurate, logic: LOGICOPT.and
        },
        {
            type: 'title', keywords: '', isFuzzy: FUZZY.accurate, logic: LOGICOPT.and
        }
    ]
    setSearchList(i: number, type: string, words: string, fuzzy: number, logic: number) {
        let list = [...this.searchList]
        if (!list[i]) list[i] = {}
        list[i].type = type
        list[i].keywords = words
        list[i].isFuzzy = fuzzy
        list[i].logic = logic
        runInAction(() => this.searchList = list)
    }
    addSearchItem() {
        let list = [...this.searchList]
        list.push({
            type: SEARCHTYPE.title,
            keywords: '',
            isFuzzy: FUZZY.accurate,
            logic: LOGICOPT.and
        })
        runInAction(() => this.searchList = list)
    }
    deleteSearchItem(i: number) {
        let list = [...this.searchList]
        list.splice(i, 1)
        runInAction(() => this.searchList = list)
    }
    async searchForList() {
        console.log('高级检索参数', JSON.parse(JSON.stringify(this.searchList)))
        let { docList } = stores
        docList.setLoading(true)
        let formdata = new FormData()
        formdata.append('page', (docList.page - 1) + '')
        formdata.append('size', docList.size + '')
        formdata.append('sort', docList.sort + '')
        formdata.append('list', JSON.stringify(this.searchList))
        let {data}=await getListBySeniorSearch(formdata)
        stores.docList.setTotal(data.total)
        stores.docList.setDocList(data.list)
        docList.setLoading(false)
    }
}