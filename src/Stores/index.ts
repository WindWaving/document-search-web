import { SeniorSearch } from './seniorSearch'
import { Classify } from './classify'
import { DocDetail } from './docDetail'
import { DocList } from './docList'
import {Search} from './search'
import { Year } from './year'
import { Source } from './source'

export const stores={
    search:new Search(),
    docList:new DocList(),
    classify:new Classify(),
    year:new Year(),
    docDetail:new DocDetail(),
    seniorSearch:new SeniorSearch(),
    source:new Source(),
};

(window as any).stores=stores