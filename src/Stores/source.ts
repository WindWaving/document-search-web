import { makeAutoObservable, runInAction } from "mobx";

export class Source {
    constructor() {
        makeAutoObservable(this)
    }
    list: any = []
    async getSourceList() {
        // let { data } = await getClassNameService()
        let data=[{
            key:'测试1',value:112
        },{
            key:'测试2',value:10
        },{
            key:'测试3',value:400
        },{
            key:'测试4',value:190
        }]
        runInAction(() => this.list = data)
    }
    setList(map: any) {
        let list: any = []
        for (let key in map) {
            if (key)
                list.push({
                    key,
                    value: map[key]
                })
        }
        runInAction(() => this.list = list)
    }
    checked: any = []
    setChecked(classes: any) {
        runInAction(() => this.checked = classes)
    }
    get parsedChecked() {
        let res = this.checked.map((name: string) => {
            return {
                type: 'source',
                value: name
            }
        })
        return res
    }
    loading:boolean=false
    setLoading(load:boolean){
        runInAction(()=>this.loading=load)
    }
    isWhole:boolean=false
    setWhole(whole:boolean){
        runInAction(()=>this.isWhole=whole)
    }
    get viewList(){
        if(this.isWhole) return this.list
        else return this.list.slice(0,4)
    }
}