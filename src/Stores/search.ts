import { makeAutoObservable, runInAction } from "mobx";
import { stores } from ".";
import { TESTLIST } from "../constant";
import { getListByAbstract, getListByAuthor, getListByContent, getListByKey, getListByProcess, getListBySource, getListByTitle } from "../Services/findList";
export const SEARCHTYPE = {
    title: 'title',
    author: 'author',
    abstracts: 'abstracts',
    content: 'content',
    source: 'source',
    keywords: 'keywords',
    classname:'classname',
}
export const SORT = {
    none: 0,
    relative: 0,
    year: 1,
}
export class Search {
    constructor() {
        makeAutoObservable(this)
    }
    searchType: string = SEARCHTYPE.keywords
    setSearchType(type: string) {
        runInAction(() => this.searchType = type)
    }
    searchValue: string = ''
    setSearchValue(val: string) {
        runInAction(() => this.searchValue = val)
    }
    resultSearchTabs: any = []
    setResultSearchTab(tabs: any) {
        runInAction(() => this.resultSearchTabs = tabs)
    }
    addResultSearchTab(key: string, value: string) {
        let tabs = [...this.resultSearchTabs]
        tabs.push({
            type: key,
            value: value
        })
        runInAction(() => this.resultSearchTabs = tabs)
    }
    deleteResultSearchTab() {
        let tabs = [...this.resultSearchTabs]
        tabs.splice(tabs.length - 1, 1)
        runInAction(() => this.resultSearchTabs = tabs)
    }
    clearResultSearch() {
        runInAction(() => this.resultSearchTabs = [])
    }
    async directSearchForList() {
        // console.log('简单搜索参数',JSON.parse(JSON.stringify(this.resultSearchTabs)) )
        let { docList,classify } = stores
        docList.setLoading(true)
        if(classify.isInit){
            stores.classify.setLoading(true)
            stores.year.setLoading(true)
            stores.source.setLoading(true)
            classify.setInit(false)
        }
        let params = {
            page: docList.page-1,
            size: docList.size,
            sort: docList.sort,
            value: this.searchValue
        }
        let res: any;
        if (this.searchType == SEARCHTYPE.keywords) {
            res = await getListByKey(params)
        }
        else if (this.searchType === SEARCHTYPE.title) {
            res = await getListByTitle(params)
        }
        else if (this.searchType === SEARCHTYPE.author) {
            res = await getListByAuthor(params)
        }
        else if (this.searchType === SEARCHTYPE.source) {
            res = await getListBySource(params)
        }
        else if (this.searchType === SEARCHTYPE.abstracts) {
            res = await getListByAbstract(params)
        }
        else if (this.searchType === SEARCHTYPE.content) {
            res = await getListByContent(params)
        }
        this.setData(res)
    }
    async resSearchForList() {
        // console.log('过程检索参数',JSON.parse(JSON.stringify(this.resultSearchTabs)) )
        let { docList, classify, year } = stores
        if(classify.isInit){
            stores.classify.setLoading(true)
            stores.year.setLoading(true)
            stores.source.setLoading(true)
            classify.setInit(false)
        }
        docList.setLoading(true)
        let tabs = [...this.resultSearchTabs, ...classify.parsedChecked, ...year.parsedChecked]
        let formdata = new FormData()
        formdata.append('page', (docList.page - 1) + '')
        formdata.append('size', docList.size + '')
        formdata.append('sort', docList.sort + '')
        formdata.append('list', JSON.stringify(tabs))

        let res = await getListByProcess(formdata)
        this.setData(res)

    }
    setData(res: any) {
        stores.classify.setClassNames(res.data.classCount)
        stores.year.setList(res.data.yearCount)
        stores.source.setList(res.data.sourceCount)
        stores.docList.setTotal(res.data.total)
        stores.docList.setDocList(res.data.list)
        stores.classify.setLoading(false)
        stores.year.setLoading(false)
        stores.source.setLoading(false)
        stores.docList.setLoading(false)
    }
}