import { makeAutoObservable, runInAction } from "mobx";

export class Year{
    constructor(){
        makeAutoObservable(this)
    }
    list:any=[]
    generateYears(){
        let res:any=[]
        let thisYear=new Date().getFullYear()
        for(let year=thisYear;year>=1995;year--){
            res.push({
                key:year,value:this.createRandValue()
            })
        }
        runInAction(()=>this.list=res)
    }
    setList(map: any) {
        let list: any = []
        for (let key in map) {
            if (key)
                list.push({
                    key,
                    value: map[key]
                })
        }
        runInAction(() => this.list = list.reverse())
    }
    checkedYears:any=[]
    setCheckedYears(years:any){
        runInAction(()=>this.checkedYears=years)
    }
    get parsedChecked(){
        return this.checkedYears.map((item:any)=>{
            return {
                type:'year',
                value:item
            }
        })
    }
    isLoading:boolean=false
    setLoading(load:boolean){
        runInAction(()=>this.isLoading=load)
    }
    isWhole:boolean=false
    setWhole(whole:boolean){
        runInAction(()=>this.isWhole=whole)
    }
    get viewList(){
        if(this.isWhole) return this.list
        else return this.list.slice(0,4)
    }

    createRandValue(){
        let val=Math.floor(Math.random()*500+10)
        return val
    }
}