import { makeAutoObservable, runInAction } from "mobx";

export class Classify {
    constructor() {
        makeAutoObservable(this)
    }
    isInit:boolean=true
    setInit(init:boolean){
        runInAction(()=>this.isInit=init)
    }
    list: any = []
    async getClassNames() {
        let data = [{
            key: '物理类', value: 100
        },
        { key: '历史类', value: 39 },
        {
            key: '哲学类', value: 392,
        }]
        runInAction(() => this.list = data)
    }
    setClassNames(map: any) {
        let list: any = []
        for (let key in map) {
            if (key)
                list.push({
                    key:key.split('类')[0],
                    value: map[key]
                })
        }
        runInAction(() => this.list = list)
    }
    isWhole:boolean=false
    setWhole(whole:boolean){
        runInAction(()=>this.isWhole=whole)
    }
    get viewList(){
        if(this.isWhole) return this.list
        else return this.list.slice(0,4)
    }
    checkedClasses: any = []
    setCheckedClass(classes: any) {
        runInAction(() => this.checkedClasses = classes)
    }
    get parsedChecked() {
        let res = this.checkedClasses.map((name: string) => {
            return {
                type: 'classname',
                value: name
            }
        })
        return res
    }
    loading:boolean=false
    setLoading(load:boolean){
        runInAction(()=>this.loading=load)
    }
}