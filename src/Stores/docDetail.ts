import { makeAutoObservable, runInAction } from "mobx";

export class DocDetail {
    constructor() {
        makeAutoObservable(this)
    }
    cur: any = {}
    setCurDoc(doc: any) {
        runInAction(() => this.cur = doc)
        for (let [k, v] of Object.entries(doc)) {
            localStorage.setItem(k, (v as string))
        }
    }
    getCurDoc() {
        let doc: any = {}
        for (let i = 0; i < localStorage.length; ++i) {
            let key = localStorage.key(i)
            if (key) {
                let val = localStorage.getItem(key)
                doc[key] = val
            }
        }
        runInAction(()=>this.cur=doc)
    }
}