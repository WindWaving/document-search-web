# document-search-web

文献检索前端

# Usage

1. git clone https地址到本地
2. 在项目根目录使用git bash执行`npm install`安装依赖
3. 第2步成功后在项目根目录执行`npm start`等待项目启动
4. 项目地址为`localhost:3000/index`，**/index一定要加上**
